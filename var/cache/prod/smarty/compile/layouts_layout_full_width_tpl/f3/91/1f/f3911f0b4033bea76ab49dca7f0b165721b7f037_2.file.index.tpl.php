<?php
/* Smarty version 3.1.33, created on 2020-08-11 23:07:49
  from 'C:\laragon\www\spray\themes\classic\templates\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5f3316b501ef83_70168061',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3911f0b4033bea76ab49dca7f0b165721b7f037' => 
    array (
      0 => 'C:\\laragon\\www\\spray\\themes\\classic\\templates\\index.tpl',
      1 => 1593780665,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f3316b501ef83_70168061 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_120544985f3316b5016189_38328773', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_11338584515f3316b5017c24_50722203 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_15145099705f3316b501ac50_72249939 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_341796845f3316b5019bc4_81857748 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15145099705f3316b501ac50_72249939', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_120544985f3316b5016189_38328773 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_120544985f3316b5016189_38328773',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_11338584515f3316b5017c24_50722203',
  ),
  'page_content' => 
  array (
    0 => 'Block_341796845f3316b5019bc4_81857748',
  ),
  'hook_home' => 
  array (
    0 => 'Block_15145099705f3316b501ac50_72249939',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11338584515f3316b5017c24_50722203', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_341796845f3316b5019bc4_81857748', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
